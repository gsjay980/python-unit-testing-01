import unittest
import Calculator07 as Calculator
from unittest.mock import patch, call


class AddNumbersTests(unittest.TestCase):
    def setUp(self):
        self.first_number = 1

        self.patched_is_number = patch('Calculator07.is_number')
        self.addCleanup(self.patched_is_number.stop)
        self.mock_is_number = self.patched_is_number.start()

    def test_add_numbers_adds_two_numbers(self):
        # Arrange
        second_number = 2
        expected = 3

        # Act
        result = Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_add_numbers_handles_adding_a_number_to_zero(self):
        # Arrange
        second_number = 0
        expected = 1

        # Act
        result = Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_add_numbers_handles_adding_a_number_to_a_negative_number(self):
        # Arrange
        second_number = -2
        expected = -1

        # Act
        result = Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_add_numbers_handles_adding_two_negative_numbers(self):
        # Arrange
        self.first_number = -1
        second_number = -2
        expected = -3

        # Act
        result = Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_add_numbers_adds_two_float_numbers(self):
        # Arrange
        self.first_number = 1.2
        second_number = 3.4
        expected = 4.6

        # Act
        result = Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_add_numbers_handles_adding_a_float_number_to_zero(self):
        # Arrange
        self.first_number = 1.2
        second_number = 0
        expected = 1.2

        # Act
        result = Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_add_numbers_handles_adding_a_float_number_to_a_negative_float_number(self):
        # Arrange
        self.first_number = 1.2
        second_number = -2.3
        expected = -1.1

        # Act
        result = Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertAlmostEqual(expected, result)

    def test_add_numbers_handles_adding_two_negative_float_numbers(self):
        # Arrange
        self.first_number = -1.2
        second_number = -2.2
        expected = -3.4

        # Act
        result = Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertAlmostEqual(expected, result)

    def test_add_numbers_handles_adding_an_integer_and_a_float_number(self):
        # Arrange
        second_number = -2.2
        expected = -1.2

        # Act
        result = Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertAlmostEqual(expected, result)

    def test_add_numbers_raises_error_when_adding_two_characters(self):
        # Arrange
        self.patched_is_number.stop()
        self.first_number = 'a'
        second_number = 'b'
        expected = 'Inputs must be numeric'

        # Act
        with self.assertRaises(ValueError) as context:
            Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, context.exception.args[0])
        self.patched_is_number.start()

    def test_add_numbers_handles_adding_a_character_and_a_number(self):
        # Arrange
        self.patched_is_number.stop()
        second_number = 'b'
        expected = 'Inputs must be numeric'

        # Act
        with self.assertRaises(ValueError) as context:
            Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, context.exception.args[0])
        self.patched_is_number.start()

    def test_add_numbers_raises_error_with_non_numeric_input(self):
        # Arrange
        self.mock_is_number.return_value = False
        second_number = -2
        expected = 'Inputs must be numeric'

        # Act
        with self.assertRaises(ValueError) as context:
            Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, context.exception.args[0])

    def test_add_numbers_raises_error_with_numeric_and_non_numeric_input(self):
        # Arrange
        self.mock_is_number.side_effect = [True, False]
        second_number = -2
        expected = 'Inputs must be numeric'

        # Act
        with self.assertRaises(ValueError) as context:
            Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, context.exception.args[0])

    def test_add_numbers_checks_for_numeric_input(self):
        # Arrange
        second_number = -2

        # Act
        Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(2, self.mock_is_number.call_count)

    def test_add_numbers_checks_for_numeric_input_against_each_argument(self):
        # Arrange
        second_number = -2
        expected = [call(self.first_number), call(second_number)]

        # Act
        Calculator.add_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertListEqual(expected, self.mock_is_number.call_args_list)


class SubtractNumbersTests(unittest.TestCase):
    def setUp(self):
        self.first_number = 1

    def test_subtract_numbers_subtracts_two_numbers(self):
        # Arrange
        second_number = 2
        expected = -1

        # Act
        result = Calculator.subtract_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_subtract_numbers_handles_subtracting_zero_from_a_number(self):
        # Arrange
        second_number = 0
        expected = 1

        # Act
        result = Calculator.subtract_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_subtract_numbers_handles_subtracting_a_number_from_zero(self):
        # Arrange
        self.first_number = 0
        second_number = 2
        expected = -2

        # Act
        result = Calculator.subtract_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_subtract_numbers_handles_subtracting_a_negative_number_from_a_number(self):
        # Arrange
        second_number = -2
        expected = 3

        # Act
        result = Calculator.subtract_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_subtract_numbers_handles_subtracting_two_negative_numbers(self):
        # Arrange
        self.first_number = -1
        second_number = -2
        expected = 1

        # Act
        result = Calculator.subtract_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)


class MultiplyNumbersTests(unittest.TestCase):
    def setUp(self):
        self.first_number = 3

    def test_multiply_numbers_multiplies_two_numbers(self):
        # Arrange
        second_number = 2
        expected = self.first_number * second_number

        # Act
        result = Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_multiply_numbers_handles_multiplying_by_1(self):
        # Arrange
        second_number = 1
        expected = self.first_number * second_number

        # Act
        result = Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_multiply_numbers_handles_multiplying_by_0(self):
        # Arrange
        second_number = 0
        expected = self.first_number * second_number

        # Act
        result = Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_multiply_numbers_handles_multiplying_by_a_negative_number(self):
        # Arrange
        second_number = -2
        expected = self.first_number * second_number

        # Act
        result = Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_multiply_numbers_handles_multiplying_a_negative_number_by_a_positive_number(self):
        # Arrange
        self.first_number = -3
        second_number = 2
        expected = self.first_number * second_number

        # Act
        result = Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_multiply_numbers_handles_multiplying_a_negative_number_by_a_negative_number(self):
        # Arrange
        self.first_number = -3
        second_number = -2
        expected = self.first_number * second_number

        # Act
        result = Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_multiply_numbers_handles_multiplying_a_non_integer_first_number(self):
        # Arrange
        self.first_number = 1.23
        second_number = -2
        expected = self.first_number * second_number

        # Act
        result = Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_multiply_numbers_handles_multiplying_a_non_integer_second_number(self):
        # Arrange
        second_number = -2.34
        error_text = 'Cannot multiply by a non-integer number'
        expected = TypeError(error_text)

        # Act
        with self.assertRaises(TypeError) as context:
            Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected.args[0], context.exception.args[0])

    def test_multiply_numbers_handles_non_numeric_first_number(self):
        # Arrange
        self.first_number = 'a'
        second_number = 2
        error_text = 'Inputs must be numeric'
        expected = ValueError(error_text)

        # Act
        with self.assertRaises(ValueError) as context:
            Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected.args[0], context.exception.args[0])


if __name__ == '__main__':
    unittest.main()
