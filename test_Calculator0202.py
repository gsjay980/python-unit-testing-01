import unittest
import Calculator02 as Calculator


class AddNumbersTests(unittest.TestCase):
    def test_add_numbers_adds_two_numbers(self):
        # Arrange
        first_number = 1
        second_number = 2
        expected = 3

        # Act
        result = Calculator.add_numbers(num1=first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_add_numbers_handles_adding_a_number_to_zero(self):
        # Arrange
        first_number = 1
        second_number = 0
        expected = 1

        # Act
        result = Calculator.add_numbers(num1=first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_add_numbers_handles_adding_a_number_to_a_negative_number(self):
        # Arrange
        first_number = 1
        second_number = -2
        expected = -1

        # Act
        result = Calculator.add_numbers(num1=first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_add_numbers_handles_adding_two_negative_numbers(self):
        # Arrange
        first_number = -1
        second_number = -2
        expected = -3

        # Act
        result = Calculator.add_numbers(num1=first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)


class SubtractNumbersTests(unittest.TestCase):
    def test_subtract_numbers_subtracts_two_numbers(self):
        # Arrange
        first_number = 1
        second_number = 2
        expected = -1

        # Act
        result = Calculator.subtract_numbers(num1=first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_subtract_numbers_handles_subtracting_zero_from_a_number(self):
        # Arrange
        first_number = 1
        second_number = 0
        expected = 1

        # Act
        result = Calculator.subtract_numbers(num1=first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_subtract_numbers_handles_subtracting_a_number_from_zero(self):
        # Arrange
        first_number = 0
        second_number = 2
        expected = -2

        # Act
        result = Calculator.subtract_numbers(num1=first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_subtract_numbers_handles_subtracting_a_negative_number_from_a_number(self):
        # Arrange
        first_number = 1
        second_number = -2
        expected = 3

        # Act
        result = Calculator.subtract_numbers(num1=first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_subtract_numbers_handles_subtracting_two_negative_numbers(self):
        # Arrange
        first_number = -1
        second_number = -2
        expected = 1

        # Act
        result = Calculator.subtract_numbers(num1=first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)


if __name__ == '__main__':
    unittest.main()
