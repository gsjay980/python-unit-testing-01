def is_number(number):
    if isinstance(number, int) or isinstance(number, float):
        return True
    else:
        return False


def add_numbers(num1, num2):
    if not is_number(num1) or not is_number(num2):
        raise ValueError('Inputs must be numeric')
    return num1 + num2


def subtract_numbers(num1, num2):
    return add_numbers(num1, -num2)


def multiply_numbers(num1, num2):
    result = 0

    if num1 < 0 and num2 < 0:
        num1 = abs(num1)
        num2 = abs(num2)
    elif num1 < 0 or num2 < 0:
        num1 = -abs(num1)
        num2 = abs(num2)

    for i in range(num2):
        result = add_numbers(result, num1)
    return result

