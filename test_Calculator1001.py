import unittest
import Calculator10 as Calculator
from unittest.mock import patch, call
from math import pow
from hypothesis import given, strategies as st


class AddNumbersTests(unittest.TestCase):
    def setUp(self):
        self.first_number = 1

    @given(st.integers(), st.integers())
    def test_add_numbers_adds_two_integers(self, num1, num2):
        # Arrange
        expected = num1 + num2

        # Act
        result = Calculator.add_numbers(num1=num1, num2=num2)
        # print(f'{num1} + {num2} = {result}')

        # Assert
        self.assertEqual(expected, result)

    @given(st.floats(allow_infinity=False, allow_nan=False), st.floats(allow_infinity=False, allow_nan=False))
    def test_add_numbers_adds_two_floats(self, num1, num2):
        # Arrange
        expected = num1 + num2

        # Act
        result = Calculator.add_numbers(num1=num1, num2=num2)
        # print(f'{num1} + {num2} = {result}')

        # Assert
        self.assertEqual(expected, result)


class SubtractNumbersTests(unittest.TestCase):
    def setUp(self):
        self.first_number = 1

    def test_subtract_numbers_subtracts_two_numbers(self):
        # Arrange
        second_number = 2
        expected = -1

        # Act
        result = Calculator.subtract_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_subtract_numbers_handles_subtracting_zero_from_a_number(self):
        # Arrange
        second_number = 0
        expected = 1

        # Act
        result = Calculator.subtract_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_subtract_numbers_handles_subtracting_a_number_from_zero(self):
        # Arrange
        self.first_number = 0
        second_number = 2
        expected = -2

        # Act
        result = Calculator.subtract_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_subtract_numbers_handles_subtracting_a_negative_number_from_a_number(self):
        # Arrange
        second_number = -2
        expected = 3

        # Act
        result = Calculator.subtract_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_subtract_numbers_handles_subtracting_two_negative_numbers(self):
        # Arrange
        self.first_number = -1
        second_number = -2
        expected = 1

        # Act
        result = Calculator.subtract_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)


class MultiplyNumbersTests(unittest.TestCase):
    def setUp(self):
        self.first_number = 3

    def test_multiply_numbers_multiplies_two_numbers(self):
        # Arrange
        second_number = 2
        expected = self.first_number * second_number

        # Act
        result = Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_multiply_numbers_handles_multiplying_by_1(self):
        # Arrange
        second_number = 1
        expected = self.first_number * second_number

        # Act
        result = Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_multiply_numbers_handles_multiplying_by_0(self):
        # Arrange
        second_number = 0
        expected = self.first_number * second_number

        # Act
        result = Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_multiply_numbers_handles_multiplying_by_a_negative_number(self):
        # Arrange
        second_number = -2
        expected = self.first_number * second_number

        # Act
        result = Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_multiply_numbers_handles_multiplying_a_negative_number_by_a_positive_number(self):
        # Arrange
        self.first_number = -3
        second_number = 2
        expected = self.first_number * second_number

        # Act
        result = Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_multiply_numbers_handles_multiplying_a_negative_number_by_a_negative_number(self):
        # Arrange
        self.first_number = -3
        second_number = -2
        expected = self.first_number * second_number

        # Act
        result = Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_multiply_numbers_handles_multiplying_a_non_integer_first_number(self):
        # Arrange
        self.first_number = 1.23
        second_number = -2
        expected = self.first_number * second_number

        # Act
        result = Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_multiply_numbers_handles_multiplying_a_non_integer_second_number(self):
        # Arrange
        second_number = -2.34
        error_text = 'Cannot multiply by a non-integer number'
        expected = TypeError(error_text)

        # Act
        with self.assertRaises(TypeError) as context:
            Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected.args[0], context.exception.args[0])

    def test_multiply_numbers_handles_non_numeric_first_number(self):
        # Arrange
        self.first_number = 'a'
        second_number = 2
        error_text = 'Inputs must be numeric'
        expected = ValueError(error_text)

        # Act
        with self.assertRaises(ValueError) as context:
            Calculator.multiply_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected.args[0], context.exception.args[0])


# noinspection PyTypeChecker
class ExponentNumbersTests(unittest.TestCase):
    def setUp(self):
        self.first_number = 3

    def test_exponent_numbers_raises_the_first_number_to_the_power_of_the_second_number(self):
        # Arrange
        second_number = 2
        expected = pow(self.first_number, second_number)

        # Act
        result = Calculator.exponent_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_exponent_numbers_handles_raising_to_the_power_zero(self):
        # Arrange
        second_number = 0
        expected = pow(self.first_number, second_number)

        # Act
        result = Calculator.exponent_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_exponent_numbers_handles_raising_zero_to_some_power(self):
        # Arrange
        self.first_number = 0
        second_number = 3
        expected = pow(self.first_number, second_number)

        # Act
        result = Calculator.exponent_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_exponent_numbers_handles_raising_to_a_negative_power(self):
        # Arrange
        second_number = -3
        expected = pow(self.first_number, second_number)

        # Act
        result = Calculator.exponent_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_exponent_numbers_handles_raising_to_a_negative_zero(self):
        # Arrange
        second_number = -0
        expected = pow(self.first_number, second_number)

        # Act
        result = Calculator.exponent_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, result)

    def test_exponent_numbers_handles_non_integer_first_number(self):
        # Arrange
        self.first_number = 1.23
        second_number = 2
        error_text = 'Cannot multiply by a non-integer number'
        expected = TypeError(error_text)

        # Act
        with self.assertRaises(TypeError) as context:
            Calculator.exponent_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected.args[0], context.exception.args[0])

    def test_exponent_numbers_handles_non_integer_second_number(self):
        # Arrange
        second_number = 2.34
        error_text = 'Cannot multiply by a non-integer number'
        expected = TypeError(error_text)

        # Act
        with self.assertRaises(TypeError) as context:
            Calculator.exponent_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected.args[0], context.exception.args[0])

    def test_exponent_numbers_handles_non_numeric_first_number(self):
        # Arrange
        self.first_number = 'a'
        second_number = 2
        error_text = 'Inputs must be numeric'
        expected = ValueError(error_text)

        # Act
        with self.assertRaises(ValueError) as context:
            Calculator.exponent_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected.args[0], context.exception.args[0])

    def test_exponent_numbers_handles_non_numeric_second_number(self):
        # Arrange
        second_number = 'b'
        error_text = 'Inputs must be numeric'
        expected = ValueError(error_text)

        # Act
        with self.assertRaises(ValueError) as context:
            Calculator.exponent_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected.args[0], context.exception.args[0])

    @patch('Calculator10.check_for_numeric_inputs')
    def test_exponent_numbers_checks_for_numeric_inputs1(self, mock_check_inputs):
        # Arrange
        second_number = 3

        # Act
        Calculator.exponent_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertTrue(mock_check_inputs.call_count > 0)

    @patch('Calculator10.multiply_numbers')
    @patch('Calculator10.check_for_numeric_inputs')
    def test_exponent_numbers_checks_for_numeric_inputs2(self, mock_check_inputs, _):
        # Arrange
        second_number = 3
        expected = 1

        # Act
        Calculator.exponent_numbers(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected, mock_check_inputs.call_count)


class CheckForNumericInputsTests(unittest.TestCase):
    def setUp(self):
        self.first_number = 3

    def test_check_for_numeric_inputs_throws_value_error_if_first_number_is_non_numeric(self):
        # Arrange
        self.first_number = 'a'
        second_number = 2
        error_text = 'Inputs must be numeric'
        expected = ValueError(error_text)

        # Act
        with self.assertRaises(ValueError) as context:
            Calculator.check_for_numeric_inputs(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected.args[0], context.exception.args[0])

    def test_check_for_numeric_inputs_throws_value_error_if_second_number_is_non_numeric(self):
        # Arrange
        second_number = 'b'
        error_text = 'Inputs must be numeric'
        expected = ValueError(error_text)

        # Act
        with self.assertRaises(ValueError) as context:
            Calculator.check_for_numeric_inputs(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected.args[0], context.exception.args[0])

    def test_check_for_numeric_inputs_throws_value_error_if_both_numbers_are_non_numeric(self):
        # Arrange
        self.first_number = 'a'
        second_number = 'b'
        error_text = 'Inputs must be numeric'
        expected = ValueError(error_text)

        # Act
        with self.assertRaises(ValueError) as context:
            Calculator.check_for_numeric_inputs(num1=self.first_number, num2=second_number)

        # Assert
        self.assertEqual(expected.args[0], context.exception.args[0])

    def test_check_for_numeric_inputs_throws_no_error_if_both_numbers_are_numeric(self):
        # Arrange
        second_number = 3
        error_thrown = False

        # Act
        try:
            Calculator.check_for_numeric_inputs(num1=self.first_number, num2=second_number)
        except ValueError:
            error_thrown = True

        # Assert
        self.assertFalse(error_thrown)


if __name__ == '__main__':
    unittest.main()
